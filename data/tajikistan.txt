﻿Importers	Select your indicators-Value exported in 2021 (USD thousand)	Select your indicators-Trade balance 2021 (USD thousand)	Select your indicators-Share in Tajikistan's exports (%)	Select your indicators-Growth in exported value between 2017-2021 (%, p.a.)	Select your indicators-Growth in exported value between 2020-2021 (%, p.a.)	Select your indicators-Ranking of partner countries in world imports	Select your indicators-Share of partner countries in world imports (%)	Select your indicators-Total imports growth in value of partner countries between 2017-2021 (%, p.a.)	Select your indicators-Average distance between partner countries and all their supplying markets (km)	Select your indicators-Concentration of all supplying countries of partner countries	Select your indicators-Average tariff (estimated) faced by Tajikistan (%)	
"World"	1786685	-2430807	100	15	36		100	3				
"Switzerland"	534977	515314	29.9	485	-10	20	1.5	4	3384	0.07		
"Kazakhstan"	360081	-458464	20.2	-5	137	58	0.2	6	3017	0.25		
"Turkey"	232168	73882	13	-3	14	22	1.2	3	4285	0.05		
"China"	159619	-520101	8.9	28	364	2	11.1	5	6416	0.04		
"Uzbekistan"	126670	-199403	7.1	10	54	74	0.1	16	3412	0.12		
"Belgium"	88773	76573	5	416	176	12	2.4	3	2709	0.08		
"Afghanistan"	82584	79651	4.6	-4	19	147	0.02	-8	2990	0.12		
"Russian Federation"	72467	-1208037	4.1	14	77	21	1.3	5	4187	0.09		
"Iran, Islamic Republic of"	38929	-43121	2.2	-8	316	72	0.1	-18	5612	0.13		
"Pakistan"	15201	-1887	0.9	2	7	47	0.3	2	4850	0.11		
"British Virgin Islands"	12969	12969	0.7	33	93	156	0.01	8	7242	0.27		
"United Arab Emirates"	10629	-9071	0.6	-7	208	29	0.9	-2	5715	0.08		
"Italy"	10554	-11832	0.6	-10	-32	11	2.5	2	2891	0.06		
"Georgia"	6606	3736	0.4	194	-50	113	0.04	-2	2513	0.09		
"Kyrgyzstan"	5555	-15068	0.3	-15	-43	127	0.03	1	2892	0.2		
"Croatia"	5353	5185	0.3		22	63	0.2	6	1362	0.07		
"Latvia"	5010	4232	0.3	17	-50	77	0.1	7	1575	0.08		
"Netherlands"	4379	-10158	0.2	-48	237	8	2.8	5	3706	0.07		
"Belarus"	3483	-34477	0.2	2	69	61	0.2	2	2268	0.23		
"Moldova, Republic of"	2388	1955	0.1	-45		118	0.03	8	2317	0.08		
"Japan"	2035	-84722	0.1	22	36	4	3.5	1	6256	0.09		
"Poland"	1443	-8443	0.1	78	72	18	1.5	8	3121	0.08		
"United Kingdom"	1055	-2699	0.1	-1	-23	7	3.2	1	3930	0.06		
"Tajikistan"	714	-13602	0	34	27	142	0.02	9	2803	0.17		
"Ukraine"	507	-25818	0	5	35	48	0.3	7	3255	0.06		
"Lithuania"	431	-9428	0	-13	2121	59	0.2	7	1630	0.07		
"Iraq"	383	-91	0	-38	-48	62	0.2	3	4107	0.18		
"Germany"	336	-106693	0	3	2	3	6.5	3	3376	0.05		
"Czech Republic"	329	-4273	0	302	9	28	1	4	2987	0.1		
"Bangladesh"	312	-1821	0		-92	49	0.3	6	4172	0.15		
"India"	193	-122221	0	-8	2632	10	2.6	2	5793	0.05		
"Austria"	90	-18575	0	-12		27	1	3	1514	0.18		
"Azerbaijan"	80	-5983	0	5	58	98	0.05	5	3712	0.09		
"United States of America"	72	-44772	0	-72	-30	1	13.4	3	7938	0.08		
"France"	62	-18910	0	-20	91	6	3.2	1	3195	0.06		
"Armenia"	57	-287	0	50	-35	131	0.02	6	3323	0.15		
"Thailand"	54	-4290	0			23	1.2	2	4905	0.09		
"Viet Nam"	52	-4128	0	11	-96	19	1.5	8	3535	0.19		
"Hong Kong, China"	35	-152	0	-16	540	5	3.3	3	2841	0.22		
"Egypt"	18	-737	0	53	-57	46	0.3	-1	5219	0.05		
"Kenya"	9	-485	0			80	0.09	2	6680	0.08		
"Slovakia"	9	-920	0		721	38	0.5	4	2664	0.07		
"Myanmar"	6	-10	0	-50	-84	79	0.10	-1	2536	0.25		
"Turkmenistan"	6	-22191	0	-83		148	0.02	4	3495	0.15		
"Macedonia, North"		-1				103	0.05	8	1348	0.09		
"Jamaica"		-1				137	0.02	-2	6759	0.22		
"Madagascar"		-1				138	0.02	2	7339	0.08		
"Palestine, State of"		-1				176	0	8	3026	0.1		
"Malta"		-2				120	0.03	1	3249	0.08		
"Cuba"		-2				145	0.02	-13	7148	0.09		
"Cambodia"		-3				66	0.1	16	2651	0.18		
"Honduras"		-3				99	0.05	4	6104	0.16		
"Iceland"		-3				112	0.04	-1	4251	0.05		
"Côte d'Ivoire"		-4				93	0.06	9	6793	0.08		
"Tanzania, United Republic of"		-4				102	0.05	7	6633	0.1		
"Uganda"		-4				132	0.02	11	5788	0.09		
"Colombia"		-5				51	0.3	4	8915	0.13		
"Eswatini"		-6				160	0	1	1597	0.74		
"Faroe Islands"		-6				172	0	7	1404	0.27		
"Mauritius"		-8				133	0.02	-2	7320	0.08		
"Barbados"		-8				161	0	0	4992	0.21		
"Seychelles"		-11				187	0	-7	6627	0.06		
"Luxembourg"		-24				71	0.1	3	1488	0.14		
"Bosnia and Herzegovina"		-25				94	0.06	3	2228	0.07		
"Curaçao"		-33				163	0	-12		0.24		
"Oman"		-35				83	0.08	-6	5977	0.08		
"Algeria"		-37				64	0.2	-8	4584	0.07		
"Botswana"		-39				124	0.03	2	2968	0.52		
"Saint Vincent and the Grenadines"		-39				196	0	3	6798	0.14		
"Uruguay"		-42				95	0.06	0	9836	0.11		
"Andorra"		-49				166	0	2	610	0.52		
"Chile"		-55				43	0.4	6	11582	0.13		
"Korea, Democratic People's Republic of"		-56				200	0	-49	1470	0.87		
"United States Minor Outlying Islands"		-65				177	0	0		0.21		
"Cyprus"		-74				105	0.05	-1	3118	0.09		
"Peru"		-85				54	0.2	3	10420	0.13		
"Greece"		-125				45	0.3	4	2847	0.05		
"Namibia"		-127				126	0.03	-1	3757	0.37		
"Area Nes"		-158										
"Norway"		-230				39	0.5	3	3470	0.06		
"Costa Rica"		-231				86	0.08	3	6987	0.22		
"Philippines"		-275				34	0.6	2	4354	0.09		
"Australia"		-280				24	1.1	1	10262	0.1		
"Morocco"		-311				52	0.3	4	4316	0.07		
"Sri Lanka"		-317				85	0.08	-3	4351	0.16		
"Antigua and Barbuda"		-346				174	0	1	6334	0.27		
"Saudi Arabia"		-348				32	0.7	3	5816	0.07		
"Anguilla"		-352				212	0	-5	4397	0.58		
"South Africa"		-366				41	0.4	-1	9230	0.07		
"Estonia"		-425				73	0.1	6	2314	0.06		
"New Zealand"		-558				56	0.2	3	11141	0.09		
"Israel"		-624				42	0.4	4	5483	0.07		
"Singapore"		-725				16	1.9	3	5773	0.07		
"Serbia"		-735				65	0.2	9	2497	0.06		
"Bulgaria"		-760				57	0.2	6	2237	0.05		
"Portugal"		-909				40	0.4	2	2788	0.14		
"Hungary"		-934				33	0.6	6	2180	0.09		
"Denmark"		-1005				35	0.6	5	2182	0.08		
"Taipei, Chinese"		-1148				17	1.7	8	5007	0.1		
"Romania"		-1253				37	0.5	6	1891	0.07		
"Ireland"		-1281				36	0.5	4	3265	0.1		
"Slovenia"		-1561				55	0.2	10	2404	0.07		
"Indonesia"		-1740				30	0.9	2	5872	0.11		
"Finland"		-2202				44	0.4	3	2842	0.07		
"Malaysia"		-2489				25	1.1	3	5172	0.09		
"Canada"		-4846				14	2.2	1	5042	0.26		
"Argentina"		-10163				50	0.3	-5	10631	0.1		
"Brazil"		-13751				26	1	6	10879	0.09		
"Spain"		-14530				15	1.9	3	3890	0.05		
"Ecuador"		-14766				69	0.1	3	9174	0.12		
"Korea, Republic of"		-40932				9	2.8	4	5699	0.09		
"Sweden"		-69902				31	0.9	3	2128	0.07		
"Mexico"						13	2.3	2	7039	0.24		
"Nigeria"						53	0.2	17	7792	0.1		
"Panama"						60	0.2	1	8657	0.12		
"Dominican Republic"						67	0.1	5	6446	0.22		
"Guatemala"						68	0.1	7	5828	0.15		
"Qatar"						70	0.1	0	5745	0.06		
"Ship stores and bunkers"						75	0.1	-11		0.16		
"Kuwait"						76	0.1	-3	5837	0.07		
"Tunisia"						78	0.10	-1	3411	0.06		
"Macao, China"						81	0.09	15		0.17		
"Ghana"						82	0.08	8	8888	0.18		
"Jordan"						84	0.08	0	5167	0.07		
"Liberia"						87	0.07	17	11608	0.19		
"El Salvador"						88	0.07	9	4914	0.15		
"Libya, State of"						89	0.07	12	3475	0.08		
"Ethiopia"						90	0.07	1	6305	0.11		
"Marshall Islands"						91	0.06	-1	7455	0.18		
"Paraguay"						92	0.06	0	7311	0.15		
"Nepal"						96	0.06	8	1856	0.56		
"Lebanon"						97	0.06	-12	3592	0.06		
"Angola"						100	0.05	-11	7692	0.06		
"Bahrain"						101	0.05	1	6359	0.08		
"Nicaragua"						104	0.05	4	5888	0.1		
"Bolivia, Plurinational State of"						106	0.04	-3	7978	0.11		
"Yemen"						107	0.04	11	5706	0.11		
"Mozambique"						108	0.04	8	6196	0.1		
"Brunei Darussalam"						109	0.04	26	5185	0.11		
"Senegal"						110	0.04	4	6632	0.05		
"Congo, Democratic Republic of the"						111	0.04	13	6479	0.14		
"Gibraltar"						114	0.03	-5	2572	0.09		
"Bahamas"						115	0.03	-8	6248	0.19		
"Cameroon"						116	0.03	8	7279	0.14		
"Venezuela, Bolivarian Republic of"						117	0.03	-11	8121	0.16		
"Zambia"						119	0.03	-9	5447	0.14		
"Albania"						121	0.03	6	1886	0.13		
"Sudan"						122	0.03	-2		0.13		
"Cayman Islands"						123	0.03	5	7522	0.35		
"Mongolia"						125	0.03	8	3267	0.22		
"Trinidad and Tobago"						128	0.03	-1	6663	0.26		
"Lao People's Democratic Republic"						129	0.02	-1	2572	0.28		
"Djibouti"						130	0.02	5	6387	0.22		
"Zimbabwe"						134	0.02	7	3754	0.39		
"Guinea"						135	0.02	7	9140	0.19		
"Burkina Faso"						136	0.02	5	6109	0.05		
"Syrian Arab Republic"						139	0.02	-3	2496	0.27		
"Haiti"						140	0.02	0	6340	0.18		
"Mali"						141	0.02	9	5008	0.1		
"Papua New Guinea"						143	0.02	-1	4973	0.17		
"Guyana"						144	0.02	13	11175	0.21		
"Mauritania"						146	0.02	1	5817	0.07		
"Benin"						149	0.01	-2	6481	0.07		
"Somalia"						150	0.01	9	5305	0.16		
"Maldives"						151	0.01	-3	4478	0.08		
"Montenegro"						152	0.01	0	2382	0.08		
"Togo"						153	0.01	10	7318	0.08		
"Congo"						154	0.01	-18	6851	0.07		
"New Caledonia"						155	0.01	-1	11471	0.2		
"Fiji"						157	0.01	-7	7384	0.12		
"Gabon"						158	0.01	-2	7114	0.11		
"Rwanda"						159	0.01	17	5437	0.08		
"Niger"						162	0	13	5715	0.09		
"Lesotho"						164	0	-2	1590	0.78		
"French Polynesia"						165	0	1	13300	0.24		
"Gambia"						167	0	7	8260	0.14		
"Malawi"						168	0	0	5169	0.15		
"Bermuda"						169	0	-3	8006	0.28		
"Sierra Leone"						170	0	10	8820	0.13		
"Saint Lucia"						171	0	-10	5321	0.23		
"Suriname"						173	0	1	9050	0.14		
"Aruba"						175	0	-3	4917	0.3		
"Bhutan"						178	0	15	1733	0.63		
"Belize"						179	0	1	5006	0.18		
"Greenland"						180	0	6	3482	0.44		
"Cabo Verde"						181	0	4	5196	0.21		
"Equatorial Guinea"						182	0	2	6008	0.11		
"Timor-Leste"						183	0	12	3988	0.22		
"Chad"						184	0	13	6931	0.16		
"Dominica"						185	0	8	4908	0.65		
"Free Zones"						186	0	-34		0.92		
"Burundi"						188	0	13	4755	0.14		
"Bonaire, Sint Eustatius and Saba"						189	0	20		0.19		
"Solomon Islands"						190	0	0	6301	0.17		
"Grenada"						191	0	-1	4157	0.2		
"Comoros"						192	0	18	5731	0.22		
"Turks and Caicos Islands"						193	0	-3	3491	0.74		
"South Sudan"						194	0	12		0.25		
"Guinea-Bissau"						195	0	2	5802	0.16		
"Samoa"						197	0	-1	6816	0.13		
"Vanuatu"						198	0	-2	6248	0.13		
"Central African Republic"						199	0	7	6606	0.08		
"Saint Kitts and Nevis"						201	0	-12	5440	0.41		
"Tonga"						202	0	4	5771	0.16		
"Eritrea"						203	0	-8	6303	0.14		
"Tuvalu"						204	0	17	6386	0.21		
"Kiribati"						205	0	10	5936	0.13		
"Cook Islands"						206	0	-4	9417	0.22		
"Falkland Islands (Malvinas)"						207	0	2	12325	0.46		
"Micronesia, Federated States of"						208	0	-8	7109	0.19		
"Palau"						209	0	1	6232	0.16		
"Sao Tome and Principe"						210	0	-3	6155	0.31		
"Nauru"						211	0	18	6014	0.27		
"Sint Maarten (Dutch part)"						213	0	-7		0.43		
"French Southern and Antarctic Territories"						214	0	14		0.12		
"Saint Pierre and Miquelon"						215	0	10	3489	0.45		
"Northern Mariana Islands"						216	0	-19	3811	0.2		
"Saint Helena"						217	0	6	7699	0.31		
"Wallis and Futuna Islands"						218	0	7	10103	0.24		
"British Indian Ocean Territory"						219	0	-9		0.67		
"Christmas Island"						220	0	3		0.69		
"Western Sahara"						221	0	-16	12469	0.9		
"Norfolk Island"						222	0	1	5454	0.24		
"Niue"						223	0	7	11410	0.48		
"Montserrat"						224	0	-7		0.49		
"Cocos (Keeling) Islands"						225	0	8		0.74		
"Tokelau"						226	0	-33	11566	0.22		
"Pitcairn"						227	0	-5		0.23		
"West Asia not elsewhere specified"						228	0			1		
"British Antarctic Territory"						229	0			1		
