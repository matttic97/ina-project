﻿Importers	Select your indicators-Value exported in 2021 (USD thousand)	Select your indicators-Trade balance 2021 (USD thousand)	Select your indicators-Share in Bangladesh's exports (%)	Select your indicators-Growth in exported value between 2017-2021 (%, p.a.)	Select your indicators-Growth in exported value between 2020-2021 (%, p.a.)	Select your indicators-Ranking of partner countries in world imports	Select your indicators-Share of partner countries in world imports (%)	Select your indicators-Total imports growth in value of partner countries between 2017-2021 (%, p.a.)	Select your indicators-Average distance between partner countries and all their supplying markets (km)	Select your indicators-Concentration of all supplying countries of partner countries	Select your indicators-Average tariff (estimated) faced by Bangladesh (%)	
"Total"	52458371	-17294005	100	5	27		100	3				
"United States of America"	8795887	6458518	16.8	8	40	1	13.4	3	7938	0.08		
"Germany"	8412792	7536406	16	6	28	3	6.5	3	3376	0.05		
"France"	3631397	3405603	6.9	5	26	6	3.2	1	3195	0.06		
"Spain"	3604790	3372402	6.9	4	22	15	1.9	3	3890	0.05		
"United Kingdom"	3298637	2738507	6.3	-3	8	7	3.2	1	3930	0.06		
"Poland"	2945567	2872846	5.6	20	30	18	1.5	8	3121	0.08		
"India"	1764100	-12328648	3.4	26	72	10	2.6	2	5793	0.05		
"Netherlands"	1695829	1280016	3.2	3	16	8	2.8	5	3706	0.07		
"Italy"	1523248	912247	2.9	-2	10	11	2.5	2	2891	0.06		
"Canada"	1505166	711922	2.9	2	28	14	2.2	1	5042	0.26		
"Japan"	1448559	-901356	2.8	3	10	4	3.5	1	6256	0.09		
"Russian Federation"	1273263	-447981	2.4	8	42	21	1.3	5	4187	0.09		
"Switzerland"	1016973	878918	1.9	17	40	20	1.5	4	3384	0.07		
"Denmark"	991588	888193	1.9	7	38	35	0.6	5	2182	0.08		
"China"	950305	-20357443	1.8	0	19	2	11.1	5	6416	0.04		
"Australia"	907412	-135460	1.7	7	24	24	1.1	1	10262	0.1		
"Belgium"	825514	611657	1.6	-7	11	12	2.4	3	2709	0.08		
"Czech Republic"	766653	718923	1.5	10	26	28	1	4	2987	0.1		
"Sweden"	629700	566825	1.2	5	21	31	0.9	3	2128	0.07		
"Korea, Republic of"	551564	-1084288	1.1	15	40	9	2.8	4	5699	0.09		
"Saudi Arabia"	490159	-441800	0.9	7	44	32	0.7	3	5816	0.07		
"Turkey"	482116	-90836	0.9	-7	-15	22	1.2	3	4285	0.05		
"Mexico"	431162	423702	0.8	9	48	13	2.3	2	7039	0.24		
"Slovakia"	375916	365821	0.7	4	30	38	0.5	4	2664	0.07		
"Norway"	353281	336797	0.7	6	30	39	0.5	3	3470	0.06		
"Malaysia"	313832	-1945271	0.6	4	51	25	1.1	3	5172	0.09		
"Austria"	234560	126250	0.4	-4	-7	27	1	3	1514	0.18		
"Ireland"	231617	188288	0.4	14	101	36	0.5	4	3265	0.1		
"Finland"	219739	177519	0.4	5	22	44	0.4	3	2842	0.07		
"Israel"	201402	201376	0.4	22	443	42	0.4	4	5483	0.07		
"Singapore"	174421	-3452897	0.3	-13	6	16	1.9	3	5773	0.07		
"Hong Kong, China"	146271	-1533299	0.3	-5	15	5	3.3	3	2841	0.22		
"Chile"	137001	120183	0.3	9	24	43	0.4	6	11582	0.13		
"New Zealand"	135784	-163924	0.3	13	32	56	0.2	3	11141	0.09		
"Slovenia"	124104	121053	0.2	2	38	55	0.2	10	2404	0.07		
"Ukraine"	123308	-127259	0.2	14	26	48	0.3	7	3255	0.06		
"South Africa"	120857	-116539	0.2	7	44	41	0.4	-1	9230	0.07		
"Brazil"	117839	-1706586	0.2	-10	-8	26	1	6	10879	0.09		
"Taipei, Chinese"	113904	-1109415	0.2	7	17	17	1.7	8	5007	0.1		
"Indonesia"	108170	-2812203	0.2	6	41	30	0.9	2	5872	0.11		
"Serbia"	107003	105206	0.2	12	37	65	0.2	9	2497	0.06		
"Portugal"	94247	75616	0.2	3	16	40	0.4	2	2788	0.14		
"Pakistan"	90423	-725190	0.2	3	46	47	0.3	2	4850	0.11		
"Colombia"	87532	86550	0.2	6	61	51	0.3	4	8915	0.13		
"Philippines"	77585	45305	0.1	19	36	34	0.6	2	4354	0.09		
"Peru"	74160	36411	0.1	8	-1	54	0.2	3	10420	0.13		
"Greece"	64986	-2611	0.1	4	31	45	0.3	4	2847	0.05		
"Morocco"	62797	-522801	0.1	6	22	52	0.3	4	4316	0.07		
"Thailand"	54075	-1162767	0.1	-4	6	23	1.2	2	4905	0.09		
"Egypt"	47632	-145452	0.1	-6	7	46	0.3	-1	5219	0.05		
"Romania"	46583	43180	0.1	6	24	37	0.5	6	1891	0.07		
"Bosnia and Herzegovina"	40425	37187	0.1	8	42	94	0.06	3	2228	0.07		
"Estonia"	37373	36914	0.1	3	38	73	0.1	6	2314	0.06		
"Luxembourg"	31101	22317	0.1	4	30	71	0.1	3	1488	0.14		
"Belarus"	23989	246	0	-13	-6	61	0.2	2	2268	0.23		
"Kenya"	20828	17343	0	17	40	80	0.09	2	6680	0.08		
"Mauritius"	20115	13106	0	30	79	133	0.02	-2	7320	0.08		
"Iceland"	19757	19531	0	13	51	112	0.04	-1	4251	0.05		
"Dominican Republic"	19532	16948	0	25	101	67	0.1	5	6446	0.22		
"Ecuador"	18472	17490	0	4	45	69	0.1	3	9174	0.12		
"Uzbekistan"	18358	7728	0	11	18	74	0.1	16	3412	0.12		
"Armenia"	16764	16763	0	-1	39	131	0.02	6	3323	0.15		
"Cyprus"	16719	-3493	0	28	194	105	0.05	-1	3118	0.09		
"Hungary"	15838	2479	0	30	32	33	0.6	6	2180	0.09		
"Croatia"	15114	12444	0	-1	1	63	0.2	6	1362	0.07		
"Argentina"	14044	-861615	0	-13	-19	50	0.3	-5	10631	0.1		
"Tunisia"	12571	-78705	0	-1	2	78	0.10	-1	3411	0.06		
"Moldova, Republic of"	12478	9123	0	7	14	118	0.03	8	2317	0.08		
"Azerbaijan"	11274	10984	0	-1	46	98	0.05	5	3712	0.09		
"Honduras"	10340	9812	0	5	112	99	0.05	4	6104	0.16		
"Cambodia"	9301	-178	0	23	-17	66	0.1	16	2651	0.18		
"Nigeria"	8533	-57831	0	37	46	53	0.2	17	7792	0.1		
"Montenegro"	7090	7090	0	-2	23	152	0.01	0	2382	0.08		
"Macao, China"	6218	6218	0	-19	1	81	0.09	15		0.17		
"Maldives"	6140	3443	0	16	61	151	0.01	-3	4478	0.08		
"Bolivia, Plurinational State of"	5316	5295	0	17	133	106	0.04	-3	7978	0.11		
"Nicaragua"	4905	4905	0	11	41	104	0.05	4	5888	0.1		
"Madagascar"	4840	4210	0	21	379	138	0.02	2	7339	0.08		
"El Salvador"	4483	3656	0	4	19	88	0.07	9	4914	0.15		
"Bulgaria"	4401	-26462	0	2	24	57	0.2	6	2237	0.05		
"Paraguay"	3886	-78407	0	25	199	92	0.06	0	7311	0.15		
"Lithuania"	3866	-1475	0	5	13	59	0.2	7	1630	0.07		
"Ethiopia"	3702	2394	0	-8	17	90	0.07	1	6305	0.11		
"Latvia"	3377	2655	0	18	182	77	0.1	7	1575	0.08		
"Malta"	3290	-3756	0	52	1790	120	0.03	1	3249	0.08		
"Tanzania, United Republic of"	2427	-7721	0	-3	-32	102	0.05	7	6633	0.1		
"Fiji"	2356	321	0	6	21	157	0.01	-7	7384	0.12		
"Georgia"	2278	2054	0	11	5	113	0.04	-2	2513	0.09		
"Tajikistan"	2133	1821	0	4	30	142	0.02	9	2803	0.17		
"Guatemala"	2003	-28997	0	20	45	68	0.1	7	5828	0.15		
"Zambia"	1961	1960	0	1	67	119	0.03	-9	5447	0.14		
"Brunei Darussalam"	1827	-21736	0	7	-36	109	0.04	26	5185	0.11		
"Kyrgyzstan"	1533	1488	0	-19	-20	127	0.03	1	2892	0.2		
"Belize"	1242	1242	0	98	-1	179	0	1	5006	0.18		
"Mozambique"	1145	267	0	-11	105	108	0.04	8	6196	0.1		
"Angola"	738	-41087	0	-9	0	100	0.05	-11	7692	0.06		
"Lao People's Democratic Republic"	685	-19463	0	13	29	129	0.02	-1	2572	0.28		
"Burkina Faso"	645	645	0	10	102	136	0.02	5	6109	0.05		
"Benin"	618	-426768	0	39	257	149	0.01	-2	6481	0.07		
"Senegal"	167	-14780	0	88	40	110	0.04	4	6632	0.05		
"Comoros"	165	165	0	40	-65	192	0	18	5731	0.22		
"Samoa"	155	66	0	33	-17	197	0	-1	6816	0.13		
"Guyana"	140	-267	0	57	55	144	0.02	13	11175	0.21		
"Togo"	138	-11523	0	-29	101	153	0.01	10	7318	0.08		
"Mauritania"	74	-78	0	-13	-68	146	0.02	1	5817	0.07		
"Barbados"	56	56	0	-33	182	161	0	0	4992	0.21		
"Grenada"	33	33	0	-1	14	191	0	-1	4157	0.2		
"Congo"		-2278				154	0.01	-18	6851	0.07		
"Viet Nam"						19	1.5	8	3535	0.19		
"United Arab Emirates"						29	0.9	-2	5715	0.08		
"Kazakhstan"						58	0.2	6	3017	0.25		
"Panama"						60	0.2	1	8657	0.12		
"Iraq"						62	0.2	3	4107	0.18		
"Algeria"						64	0.2	-8	4584	0.07		
"Qatar"						70	0.1	0	5745	0.06		
"Iran, Islamic Republic of"						72	0.1	-18	5612	0.13		
"Ship stores and bunkers"						75	0.1	-11		0.16		
"Kuwait"						76	0.1	-3	5837	0.07		
"Myanmar"						79	0.10	-1	2536	0.25		
"Ghana"						82	0.08	8	8888	0.18		
"Oman"						83	0.08	-6	5977	0.08		
"Jordan"						84	0.08	0	5167	0.07		
"Sri Lanka"						85	0.08	-3	4351	0.16		
"Costa Rica"						86	0.08	3	6987	0.22		
"Liberia"						87	0.07	17	11608	0.19		
"Libya, State of"						89	0.07	12	3475	0.08		
"Marshall Islands"						91	0.06	-1	7455	0.18		
"Côte d'Ivoire"						93	0.06	9	6793	0.08		
"Uruguay"						95	0.06	0	9836	0.11		
"Nepal"						96	0.06	8	1856	0.56		
"Lebanon"						97	0.06	-12	3592	0.06		
"Bahrain"						101	0.05	1	6359	0.08		
"Macedonia, North"						103	0.05	8	1348	0.09		
"Yemen"						107	0.04	11	5706	0.11		
"Congo, Democratic Republic of the"						111	0.04	13	6479	0.14		
"Gibraltar"						114	0.03	-5	2572	0.09		
"Bahamas"						115	0.03	-8	6248	0.19		
"Cameroon"						116	0.03	8	7279	0.14		
"Venezuela, Bolivarian Republic of"						117	0.03	-11	8121	0.16		
"Albania"						121	0.03	6	1886	0.13		
"Sudan"						122	0.03	-2		0.13		
"Cayman Islands"						123	0.03	5	7522	0.35		
"Botswana"						124	0.03	2	2968	0.52		
"Mongolia"						125	0.03	8	3267	0.22		
"Namibia"						126	0.03	-1	3757	0.37		
"Trinidad and Tobago"						128	0.03	-1	6663	0.26		
"Djibouti"						130	0.02	5	6387	0.22		
"Uganda"						132	0.02	11	5788	0.09		
"Zimbabwe"						134	0.02	7	3754	0.39		
"Guinea"						135	0.02	7	9140	0.19		
"Jamaica"						137	0.02	-2	6759	0.22		
"Syrian Arab Republic"						139	0.02	-3	2496	0.27		
"Haiti"						140	0.02	0	6340	0.18		
"Mali"						141	0.02	9	5008	0.1		
"Papua New Guinea"						143	0.02	-1	4973	0.17		
"Cuba"						145	0.02	-13	7148	0.09		
"Afghanistan"						147	0.02	-8	2990	0.12		
"Turkmenistan"						148	0.02	4	3495	0.15		
"Somalia"						150	0.01	9	5305	0.16		
"New Caledonia"						155	0.01	-1	11471	0.2		
"British Virgin Islands"						156	0.01	8	7242	0.27		
"Gabon"						158	0.01	-2	7114	0.11		
"Rwanda"						159	0.01	17	5437	0.08		
"Eswatini"						160	0	1	1597	0.74		
"Niger"						162	0	13	5715	0.09		
"Curaçao"						163	0	-12		0.24		
"Lesotho"						164	0	-2	1590	0.78		
"French Polynesia"						165	0	1	13300	0.24		
"Andorra"						166	0	2	610	0.52		
"Gambia"						167	0	7	8260	0.14		
"Malawi"						168	0	0	5169	0.15		
"Bermuda"						169	0	-3	8006	0.28		
"Sierra Leone"						170	0	10	8820	0.13		
"Saint Lucia"						171	0	-10	5321	0.23		
"Faroe Islands"						172	0	7	1404	0.27		
"Suriname"						173	0	1	9050	0.14		
"Antigua and Barbuda"						174	0	1	6334	0.27		
"Aruba"						175	0	-3	4917	0.3		
"Palestine, State of"						176	0	8	3026	0.1		
"United States Minor Outlying Islands"						177	0	0		0.21		
"Bhutan"						178	0	15	1733	0.63		
"Greenland"						180	0	6	3482	0.44		
"Cabo Verde"						181	0	4	5196	0.21		
"Equatorial Guinea"						182	0	2	6008	0.11		
"Timor-Leste"						183	0	12	3988	0.22		
"Chad"						184	0	13	6931	0.16		
"Dominica"						185	0	8	4908	0.65		
"Free Zones"						186	0	-34		0.92		
"Seychelles"						187	0	-7	6627	0.06		
"Burundi"						188	0	13	4755	0.14		
"Bonaire, Sint Eustatius and Saba"						189	0	20		0.19		
"Solomon Islands"						190	0	0	6301	0.17		
"Turks and Caicos Islands"						193	0	-3	3491	0.74		
"South Sudan"						194	0	12		0.25		
"Guinea-Bissau"						195	0	2	5802	0.16		
"Saint Vincent and the Grenadines"						196	0	3	6798	0.14		
"Vanuatu"						198	0	-2	6248	0.13		
"Central African Republic"						199	0	7	6606	0.08		
"Korea, Democratic People's Republic of"						200	0	-49	1470	0.87		
"Saint Kitts and Nevis"						201	0	-12	5440	0.41		
"Tonga"						202	0	4	5771	0.16		
"Eritrea"						203	0	-8	6303	0.14		
"Tuvalu"						204	0	17	6386	0.21		
"Kiribati"						205	0	10	5936	0.13		
"Cook Islands"						206	0	-4	9417	0.22		
"Falkland Islands (Malvinas)"						207	0	2	12325	0.46		
"Micronesia, Federated States of"						208	0	-8	7109	0.19		
"Palau"						209	0	1	6232	0.16		
"Sao Tome and Principe"						210	0	-3	6155	0.31		
"Nauru"						211	0	18	6014	0.27		
"Anguilla"						212	0	-5	4397	0.58		
"Sint Maarten (Dutch part)"						213	0	-7		0.43		
"French Southern and Antarctic Territories"						214	0	14		0.12		
"Saint Pierre and Miquelon"						215	0	10	3489	0.45		
"Northern Mariana Islands"						216	0	-19	3811	0.2		
"Saint Helena"						217	0	6	7699	0.31		
"Wallis and Futuna Islands"						218	0	7	10103	0.24		
"British Indian Ocean Territory"						219	0	-9		0.67		
"Christmas Island"						220	0	3		0.69		
"Western Sahara"						221	0	-16	12469	0.9		
"Norfolk Island"						222	0	1	5454	0.24		
"Niue"						223	0	7	11410	0.48		
"Montserrat"						224	0	-7		0.49		
"Cocos (Keeling) Islands"						225	0	8		0.74		
"Tokelau"						226	0	-33	11566	0.22		
"Pitcairn"						227	0	-5		0.23		
"West Asia not elsewhere specified"						228	0			1		
"British Antarctic Territory"						229	0			1		
