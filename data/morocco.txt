﻿Importers	Select your indicators-Value exported in 2021 (USD thousand)	Select your indicators-Trade balance 2021 (USD thousand)	Select your indicators-Share in Morocco's exports (%)	Select your indicators-Growth in exported value between 2017-2021 (%, p.a.)	Select your indicators-Growth in exported value between 2020-2021 (%, p.a.)	Select your indicators-Ranking of partner countries in world imports	Select your indicators-Share of partner countries in world imports (%)	Select your indicators-Total imports growth in value of partner countries between 2017-2021 (%, p.a.)	Select your indicators-Average distance between partner countries and all their supplying markets (km)	Select your indicators-Concentration of all supplying countries of partner countries	Select your indicators-Average tariff (estimated) faced by Morocco (%)	
"World"	36578743	-22088514	100	7	32		100	3				
"Spain"	7865015	-1363014	21.5	5	19	15	1.9	3	3890	0.05		
"France"	7516064	1327071	20.5	5	24	6	3.2	1	3195	0.06		
"Brazil"	2029315	1180431	5.5	27	78	26	1	6	10879	0.09		
"India"	1806734	736652	4.9	23	52	10	2.6	2	5793	0.05		
"Italy"	1559790	-1413504	4.3	5	28	11	2.5	2	2891	0.06		
"United Kingdom"	1197087	469520	3.3	10	111	7	3.2	1	3930	0.06		
"United States of America"	1103262	-2642357	3	-2	12	1	13.4	3	7938	0.08		
"Germany"	1075011	-1499514	2.9	8	19	3	6.5	3	3376	0.05		
"Netherlands"	926633	20598	2.5	14	6	8	2.8	5	3706	0.07		
"Turkey"	800044	-2581820	2.2	3	33	22	1.2	3	4285	0.05		
"Bangladesh"	585598	522801	1.6	45	422	49	0.3	6	4172	0.15		
"Belgium"	552707	-503050	1.5	5	38	12	2.4	3	2709	0.08		
"Pakistan"	545623	502575	1.5	18	73	47	0.3	2	4850	0.11		
"Portugal"	512503	-1126369	1.4	5	39	40	0.4	2	2788	0.14		
"Area Nes"	483642	381407	1.3	4	83							
"China"	362463	-6508016	1	3	39	2	11.1	5	6416	0.04		
"Côte d'Ivoire"	358446	331437	1	20	43	93	0.06	9	6793	0.08		
"Poland"	351771	-339915	1	5	61	18	1.5	8	3121	0.08		
"Senegal"	311059	295382	0.9	11	46	110	0.04	4	6632	0.05		
"Mexico"	305810	144822	0.8	17	74	13	2.3	2	7039	0.24		
"Argentina"	302822	-525323	0.8	36	29	50	0.3	-5	10631	0.1		
"Switzerland"	271317	-45895	0.7	12	70	20	1.5	4	3384	0.07		
"Mauritania"	261981	261557	0.7	7	42	146	0.02	1	5817	0.07		
"Djibouti"	257302	257301	0.7	66	3	130	0.02	5	6387	0.22		
"Australia"	256885	222135	0.7	83	474	24	1.1	1	10262	0.1		
"Romania"	251917	-506743	0.7	9	90	37	0.5	6	1891	0.07		
"Canada"	216327	-355955	0.6	1	6	14	2.2	1	5042	0.26		
"Japan"	194416	-239622	0.5	-3	15	4	3.5	1	6256	0.09		
"Austria"	193725	-93748	0.5	-2	13	27	1	3	1514	0.18		
"Russian Federation"	188510	-1827024	0.5	-4	-9	21	1.3	5	4187	0.09		
"Czech Republic"	156596	-360274	0.4	4	113	28	1	4	2987	0.1		
"Singapore"	155623	6945	0.4	-14	-15	16	1.9	3	5773	0.07		
"Tunisia"	144158	-109457	0.4	7	62	78	0.10	-1	3411	0.06		
"Ghana"	132840	126969	0.4	6	4	82	0.08	8	8888	0.18		
"United Arab Emirates"	132111	-891129	0.4	13	43	29	0.9	-2	5715	0.08		
"Nigeria"	130889	99313	0.4	-7	25	53	0.2	17	7792	0.1		
"Sweden"	130808	-214256	0.4	6	133	31	0.9	3	2128	0.07		
"Algeria"	114837	-534361	0.3	-13	-14	64	0.2	-8	4584	0.07		
"Mali"	112174	108074	0.3	9	73	141	0.02	9	5008	0.1		
"Cameroon"	98377	93441	0.3	7	55	116	0.03	8	7279	0.14		
"Hong Kong, China"	98017	78142	0.3	58	1206	5	3.3	3	2841	0.22		
"Guinea"	97059	91064	0.3	10	10	135	0.02	7	9140	0.19		
"Ireland"	95692	-74096	0.3	-10	-23	36	0.5	4	3265	0.1		
"Benin"	94833	94629	0.3	3	139	149	0.01	-2	6481	0.07		
"Burkina Faso"	88158	87017	0.2	11	44	136	0.02	5	6109	0.05		
"Greece"	86816	-87917	0.2	22	32	45	0.3	4	2847	0.05		
"Saudi Arabia"	85435	-1729535	0.2	-10	-12	32	0.7	3	5816	0.07		
"Ukraine"	84447	-494045	0.2	41	50	48	0.3	7	3255	0.06		
"Egypt"	83077	-736454	0.2	7	-8	46	0.3	-1	5219	0.05		
"Slovenia"	79729	11972	0.2	26	153	55	0.2	10	2404	0.07		
"Denmark"	77174	-57649	0.2	20	29	35	0.6	5	2182	0.08		
"Libya, State of"	76750	40316	0.2	-2	13	89	0.07	12	3475	0.08		
"New Zealand"	75810	39219	0.2	14	245	56	0.2	3	11141	0.09		
"Norway"	72283	-29659	0.2	12	26	39	0.5	3	3470	0.06		
"Slovakia"	69257	-177174	0.2	7	15	38	0.5	4	2664	0.07		
"Hungary"	69244	-226333	0.2	11	-7	33	0.6	6	2180	0.09		
"Bulgaria"	59990	-127658	0.2	-24	-6	57	0.2	6	2237	0.05		
"Korea, Republic of"	57826	-358832	0.2	-14	3	9	2.8	4	5699	0.09		
"Mozambique"	57450	56252	0.2	1	36	108	0.04	8	6196	0.1		
"Lithuania"	54643	-7790	0.1	91	69	59	0.2	7	1630	0.07		
"Togo"	53119	7784	0.1	5	-2	153	0.01	10	7318	0.08		
"Indonesia"	47870	-70373	0.1	1	90	30	0.9	2	5872	0.11		
"South Africa"	46937	-99250	0.1	1	18	41	0.4	-1	9230	0.07		
"Belarus"	45735	-33919	0.1	58	140	61	0.2	2	2268	0.23		
"Uruguay"	44717	39771	0.1	21	32	95	0.06	0	9836	0.11		
"Gabon"	43825	17764	0.1	0	-16	158	0.01	-2	7114	0.11		
"Lebanon"	41686	21467	0.1	-14	-12	97	0.06	-12	3592	0.06		
"Peru"	41114	-12441	0.1	-11	78	54	0.2	3	10420	0.13		
"Syrian Arab Republic"	38651	36167	0.1	-5	-19	139	0.02	-3	2496	0.27		
"Ethiopia"	34339	30964	0.1	-47	2130	90	0.07	1	6305	0.11		
"Congo"	32271	29119	0.1	-11	19	154	0.01	-18	6851	0.07		
"Taipei, Chinese"	30408	-101667	0.1	-9	68	17	1.7	8	5007	0.1		
"Qatar"	30004	-126498	0.1	2	-9	70	0.1	0	5745	0.06		
"Finland"	29264	-152804	0.1	33	265	44	0.4	3	2842	0.07		
"Niger"	28662	28645	0.1	2	-5	162	0	13	5715	0.09		
"Kenya"	26749	5776	0.1	16	91	80	0.09	2	6680	0.08		
"Angola"	24130	3998	0.1	-19	72	100	0.05	-11	7692	0.06		
"Congo, Democratic Republic of the"	23410	16372	0.1	42	21	111	0.04	13	6479	0.14		
"Croatia"	22246	3767	0.1	4	-27	63	0.2	6	1362	0.07		
"Namibia"	21209	21077	0.1	2	51	126	0.03	-1	3757	0.37		
"Sierra Leone"	19124	18534	0.1	9	89	170	0	10	8820	0.13		
"Malta"	18008	16334	0	-3	108	120	0.03	1	3249	0.08		
"Colombia"	17725	-22384	0	22	-14	51	0.3	4	8915	0.13		
"Chile"	16436	-23566	0	5	78	43	0.4	6	11582	0.13		
"Macedonia, North"	16315	11506	0	120	443	103	0.05	8	1348	0.09		
"Gambia"	16074	15466	0	8	33	167	0	7	8260	0.14		
"Equatorial Guinea"	14245	14039	0	6	49	182	0	2	6008	0.11		
"Kuwait"	13136	-19747	0	-30	16	76	0.1	-3	5837	0.07		
"Honduras"	13031	11548	0	118	17502	99	0.05	4	6104	0.16		
"Liberia"	12088	8830	0	28	146	87	0.07	17	11608	0.19		
"Viet Nam"	11530	-322304	0	-5	166	19	1.5	8	3535	0.19		
"Malaysia"	11128	-161179	0	15	-45	25	1.1	3	5172	0.09		
"Tanzania, United Republic of"	10907	-5249	0	-16	-65	102	0.05	7	6633	0.1		
"Thailand"	9455	-152910	0	-21	-40	23	1.2	2	4905	0.09		
"Jordan"	8913	-44536	0	-20	-69	84	0.08	0	5167	0.07		
"Chad"	8387	8360	0	35	20	184	0	13	6931	0.16		
"Oman"	7763	-11500	0	14	-81	83	0.08	-6	5977	0.08		
"Guinea-Bissau"	7455	7365	0	13	103	195	0	2	5802	0.16		
"Palestine, State of"	7136	6717	0	15	43	176	0	8	3026	0.1		
"Madagascar"	6531	-5012	0	-4	-8	138	0.02	2	7339	0.08		
"Serbia"	6248	-7858	0	26	-90	65	0.2	9	2497	0.06		
"Luxembourg"	5997	-11257	0	2	6	71	0.1	3	1488	0.14		
"Sudan"	5941	5534	0	-36	13	122	0.03	-2		0.13		
"Iceland"	5671	4963	0	33	105	112	0.04	-1	4251	0.05		
"Iraq"	5645	1273	0	-25	13	62	0.2	3	4107	0.18		
"Mauritius"	5109	4978	0	-5	-5	133	0.02	-2	7320	0.08		
"Rwanda"	4511	4301	0	20	82	159	0.01	17	5437	0.08		
"Ecuador"	4436	-26051	0	14	2259	69	0.1	3	9174	0.12		
"Yemen"	4134	4134	0	-2	10	107	0.04	11	5706	0.11		
"Paraguay"	4054	3812	0	64	-71	92	0.06	0	7311	0.15		
"Central African Republic"	4029	2571	0	0	-3	199	0	7	6606	0.08		
"Dominican Republic"	3967	3705	0	7	-46	67	0.1	5	6446	0.22		
"Uganda"	3965	-28428	0	15	42	132	0.02	11	5788	0.09		
"Trinidad and Tobago"	3941	-279587	0	3	25	128	0.03	-1	6663	0.26		
"Bahrain"	3249	-128125	0	23	-90	101	0.05	1	6359	0.08		
"Albania"	2836	1752	0	9	5	121	0.03	6	1886	0.13		
"Cabo Verde"	2712	2165	0	31	81	181	0	4	5196	0.21		
"Sri Lanka"	2632	-13445	0	-20	32	85	0.08	-3	4351	0.16		
"Comoros"	2565	2565	0	7	15	192	0	18	5731	0.22		
"Cyprus"	2469	861	0	5	22	105	0.05	-1	3118	0.09		
"Bosnia and Herzegovina"	2375	-4995	0	-10	-39	94	0.06	3	2228	0.07		
"Georgia"	2231	-2530	0	81	-56	113	0.04	-2	2513	0.09		
"Armenia"	1969	1913	0	51	1820	131	0.02	6	3323	0.15		
"Somalia"	1806	-797	0	-16	274	150	0.01	9	5305	0.16		
"New Caledonia"	1752	1749	0	9	-29	155	0.01	-1	11471	0.2		
"Estonia"	1667	-21143	0	-17	-31	73	0.1	6	2314	0.06		
"Philippines"	1660	-27706	0	-21	-72	34	0.6	2	4354	0.09		
"Latvia"	1556	-21963	0	-10	-58	77	0.1	7	1575	0.08		
"Haiti"	1308	1307	0	-33	42	140	0.02	0	6340	0.18		
"Costa Rica"	934	-9915	0	-11	-20	86	0.08	3	6987	0.22		
"Venezuela, Bolivarian Republic of"	922	-1235	0	-51	-22	117	0.03	-11	8121	0.16		
"Barbados"	778	686	0	195	43056	161	0	0	4992	0.21		
"Guyana"	675	640	0	59	20	144	0.02	13	11175	0.21		
"Zambia"	648	-302	0	172	114	119	0.03	-9	5447	0.14		
"Burundi"	596	464	0	20	60	188	0	13	4755	0.14		
"Turkmenistan"	553	-50053	0		152	148	0.02	4	3495	0.15		
"Botswana"	535	535	0	68	192	124	0.03	2	2968	0.52		
"Jamaica"	515	484	0	23		137	0.02	-2	6759	0.22		
"Guatemala"	498	-26063	0	34	-93	68	0.1	7	5828	0.15		
"Eswatini"	477	-4302	0	34	315	160	0	1	1597	0.74		
"El Salvador"	425	316	0	-3	-66	88	0.07	9	4914	0.15		
"Nepal"	422	278	0		1289	96	0.06	8	1856	0.56		
"French Polynesia"	402	402	0	-12	-54	165	0	1	13300	0.24		
"Suriname"	384	-1795	0	-1	-18	173	0	1	9050	0.14		
"Panama"	362	305	0	-28	604	60	0.2	1	8657	0.12		
"Azerbaijan"	312	312	0	0	-57	98	0.05	5	3712	0.09		
"Curaçao"	183	183	0	10	-56	163	0	-12		0.24		
"Kazakhstan"	143	-215672	0	-18	-86	58	0.2	6	3017	0.25		
"Montenegro"	124	-719	0	9	-16	152	0.01	0	2382	0.08		
"Seychelles"	106	-311	0	-10	311	187	0	-7	6627	0.06		
"Fiji"	87	87	0		2	157	0.01	-7	7384	0.12		
"Gibraltar"	86	41	0	-14	3381	114	0.03	-5	2572	0.09		
"Maldives"	85	77	0	3	-21	151	0.01	-3	4478	0.08		
"Andorra"	67	67	0	-39	-83	166	0	2	610	0.52		
"Mongolia"	52	-68	0	-29		125	0.03	8	3267	0.22		
"Bahamas"	49	46	0	16	-34	115	0.03	-8	6248	0.19		
"Moldova, Republic of"	45	-6518	0	-39	246	118	0.03	8	2317	0.08		
"Afghanistan"	44	-328	0	-34	11	147	0.02	-8	2990	0.12		
"Iran, Islamic Republic of"	41	-9723	0	-76	1448	72	0.1	-18	5612	0.13		
"Brunei Darussalam"	36	36	0			109	0.04	26	5185	0.11		
"Sao Tome and Principe"	29	29	0	-60	-1	210	0	-3	6155	0.31		
"Cambodia"	29	-5942	0	-46	-60	66	0.1	16	2651	0.18		
"Zimbabwe"	26	25	0	105	-89	134	0.02	7	3754	0.39		
"Macao, China"	9	-10	0	-53	27	81	0.09	15		0.17		
"Saint Vincent and the Grenadines"	4	4	0			196	0	3	6798	0.14		
"Nicaragua"	4	-107	0	287	-100	104	0.05	4	5888	0.1		
"Cuba"	4	-791	0	-80	-99	145	0.02	-13	7148	0.09		
"Marshall Islands"	1	1	0			91	0.06	-1	7455	0.18		
"United States Minor Outlying Islands"		-2				177	0	0		0.21		
"British Virgin Islands"		-3				156	0.01	8	7242	0.27		
"Tokelau"		-3				226	0	-33	11566	0.22		
"Eritrea"		-4				203	0	-8	6303	0.14		
"Lesotho"		-8				164	0	-2	1590	0.78		
"Solomon Islands"		-9				190	0	0	6301	0.17		
"Faroe Islands"		-12				172	0	7	1404	0.27		
"Saint Lucia"		-27				171	0	-10	5321	0.23		
"Belize"		-48				179	0	1	5006	0.18		
"Bermuda"		-54				169	0	-3	8006	0.28		
"Dominica"		-56				185	0	8	4908	0.65		
"Lao People's Democratic Republic"		-209				129	0.02	-1	2572	0.28		
"Kyrgyzstan"		-277				127	0.03	1	2892	0.2		
"Tajikistan"		-332				142	0.02	9	2803	0.17		
"Bolivia, Plurinational State of"		-335				106	0.04	-3	7978	0.11		
"Sint Maarten (Dutch part)"		-1847				213	0	-7		0.43		
"Myanmar"		-4105				79	0.10	-1	2536	0.25		
"Malawi"		-4162				168	0	0	5169	0.15		
"Greenland"		-5214				180	0	6	3482	0.44		
"Uzbekistan"		-9677				74	0.1	16	3412	0.12		
"Falkland Islands (Malvinas)"		-21584				207	0	2	12325	0.46		
"Israel"						42	0.4	4	5483	0.07		
"Ship stores and bunkers"						75	0.1	-11		0.16		
"Cayman Islands"						123	0.03	5	7522	0.35		
"Papua New Guinea"						143	0.02	-1	4973	0.17		
"Antigua and Barbuda"						174	0	1	6334	0.27		
"Aruba"						175	0	-3	4917	0.3		
"Bhutan"						178	0	15	1733	0.63		
"Timor-Leste"						183	0	12	3988	0.22		
"Free Zones"						186	0	-34		0.92		
"Bonaire, Sint Eustatius and Saba"						189	0	20		0.19		
"Grenada"						191	0	-1	4157	0.2		
"Turks and Caicos Islands"						193	0	-3	3491	0.74		
"South Sudan"						194	0	12		0.25		
"Samoa"						197	0	-1	6816	0.13		
"Vanuatu"						198	0	-2	6248	0.13		
"Korea, Democratic People's Republic of"						200	0	-49	1470	0.87		
"Saint Kitts and Nevis"						201	0	-12	5440	0.41		
"Tonga"						202	0	4	5771	0.16		
"Tuvalu"						204	0	17	6386	0.21		
"Kiribati"						205	0	10	5936	0.13		
"Cook Islands"						206	0	-4	9417	0.22		
"Micronesia, Federated States of"						208	0	-8	7109	0.19		
"Palau"						209	0	1	6232	0.16		
"Nauru"						211	0	18	6014	0.27		
"Anguilla"						212	0	-5	4397	0.58		
"French Southern and Antarctic Territories"						214	0	14		0.12		
"Saint Pierre and Miquelon"						215	0	10	3489	0.45		
"Northern Mariana Islands"						216	0	-19	3811	0.2		
"Saint Helena"						217	0	6	7699	0.31		
"Wallis and Futuna Islands"						218	0	7	10103	0.24		
"British Indian Ocean Territory"						219	0	-9		0.67		
"Christmas Island"						220	0	3		0.69		
"Western Sahara"						221	0	-16	12469	0.9		
"Norfolk Island"						222	0	1	5454	0.24		
"Niue"						223	0	7	11410	0.48		
"Montserrat"						224	0	-7		0.49		
"Cocos (Keeling) Islands"						225	0	8		0.74		
"Pitcairn"						227	0	-5		0.23		
"West Asia not elsewhere specified"						228	0			1		
"British Antarctic Territory"						229	0			1		
