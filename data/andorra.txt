﻿Importers	Select your indicators-Value exported in 2021 (USD thousand)	Select your indicators-Trade balance 2021 (USD thousand)	Select your indicators-Share in Andorra's exports (%)	Select your indicators-Growth in exported value between 2017-2021 (%, p.a.)	Select your indicators-Growth in exported value between 2020-2021 (%, p.a.)	Select your indicators-Ranking of partner countries in world imports	Select your indicators-Share of partner countries in world imports (%)	Select your indicators-Total imports growth in value of partner countries between 2017-2021 (%, p.a.)	Select your indicators-Average distance between partner countries and all their supplying markets (km)	Select your indicators-Concentration of all supplying countries of partner countries	Select your indicators-Average tariff (estimated) faced by Andorra (%)	
"Total"	115896	-1450215	100	5	0		100	3				
"Spain"	34007	-1061563	29.3	2	-11	15	1.9	3	3890	0.05		
"France"	29070	-221380	25.1	22	-16	6	3.2	1	3195	0.06		
"Nigeria"	8564	8564	7.4	128	383	53	0.2	17	7792	0.1		
"United States of America"	8226	3536	7.1	-19	49	1	13.4	3	7938	0.08		
"El Salvador"	7470	7170	6.4	203	87	88	0.07	9	4914	0.15		
"United Kingdom"	5805	-439	5	79	819	7	3.2	1	3930	0.06		
"Netherlands"	4019	-14873	3.5	2	77	8	2.8	5	3706	0.07		
"Germany"	2675	-70875	2.3	-10	-55	3	6.5	3	3376	0.05		
"Belgium"	923	-12263	0.8	9	82	12	2.4	3	2709	0.08		
"Switzerland"	862	-2358	0.7	19	19	20	1.5	4	3384	0.07		
"Philippines"	845	220	0.7	-47	35675	34	0.6	2	4354	0.09		
"Norway"	818	753	0.7	-28	-44	39	0.5	3	3470	0.06		
"Czech Republic"	803	-1088	0.7	27	300	28	1	4	2987	0.1		
"Finland"	785	61	0.7	-15	-52	44	0.4	3	2842	0.07		
"Honduras"	730	730	0.6	57	231	99	0.05	4	6104	0.16		
"Saudi Arabia"	700	700	0.6		1786	32	0.7	3	5816	0.07		
"China"	693	-2912	0.6	65	60	2	11.1	5	6416	0.04		
"Singapore"	660	620	0.6	19	5	16	1.9	3	5773	0.07		
"Dominican Republic"	650	586	0.6	85		67	0.1	5	6446	0.22		
"Poland"	502	-5514	0.4	-37	156	18	1.5	8	3121	0.08		
"Italy"	475	-30269	0.4	-10	-26	11	2.5	2	2891	0.06		
"Greece"	467	187	0.4	13	-9	45	0.3	4	2847	0.05		
"Israel"	417	417	0.4	84	-66	42	0.4	4	5483	0.07		
"Malaysia"	384	125	0.3	-17	17905	25	1.1	3	5172	0.09		
"Serbia"	322	321	0.3	7	8135	65	0.2	9	2497	0.06		
"South Africa"	290	-76	0.3	11	154	41	0.4	-1	9230	0.07		
"Hong Kong, China"	290	-1629	0.3	59	-97	5	3.3	3	2841	0.22		
"Ethiopia"	285	-2697	0.2	54	32	90	0.07	1	6305	0.11		
"Guatemala"	276	276	0.2	-11	17	68	0.1	7	5828	0.15		
"Korea, Republic of"	252	-830	0.2	4	219	9	2.8	4	5699	0.09		
"Portugal"	237	-8973	0.2	19	-29	40	0.4	2	2788	0.14		
"Japan"	210	-565	0.2	100	54	4	3.5	1	6256	0.09		
"Romania"	184	-354	0.2	43	35	37	0.5	6	1891	0.07		
"Ukraine"	181	73	0.2	105	2244	48	0.3	7	3255	0.06		
"Ireland"	180	-179	0.2	43	305	36	0.5	4	3265	0.1		
"Luxembourg"	180	-2206	0.2	1	114	71	0.1	3	1488	0.14		
"Fiji"	172	172	0.1			157	0.01	-7	7384	0.12		
"Mexico"	171	171	0.1	-50	-40	13	2.3	2	7039	0.24		
"Taipei, Chinese"	169	-237	0.1	-3	1175	17	1.7	8	5007	0.1		
"Zambia"	154	154	0.1	18	-2	119	0.03	-9	5447	0.14		
"Slovakia"	150	-156	0.1	3	-70	38	0.5	4	2664	0.07		
"Mauritania"	144	144	0.1	-53	2254	146	0.02	1	5817	0.07		
"Thailand"	127	-8924	0.1	-6	94	23	1.2	2	4905	0.09		
"Indonesia"	125	-92	0.1	23	446	30	0.9	2	5872	0.11		
"Austria"	116	-5942	0.1	-31	-18	27	1	3	1514	0.18		
"Canada"	115	-254	0.1	-38	17	14	2.2	1	5042	0.26		
"Slovenia"	102	-15	0.1	18	5	55	0.2	10	2404	0.07		
"Moldova, Republic of"	100	100	0.1	1	33	118	0.03	8	2317	0.08		
"Brazil"	87	-57	0.1	33	224	26	1	6	10879	0.09		
"Denmark"	75	-593	0.1	57	317	35	0.6	5	2182	0.08		
"Colombia"	69	69	0.1	-45	-60	51	0.3	4	8915	0.13		
"Bosnia and Herzegovina"	58	58	0.1	69	131	94	0.06	3	2228	0.07		
"Russian Federation"	50	-13428	0	75	27	21	1.3	5	4187	0.09		
"Tajikistan"	49	49	0	-12	4	142	0.02	9	2803	0.17		
"Bulgaria"	44	-10	0	10	53	57	0.2	6	2237	0.05		
"Montenegro"	42	42	0	-20	-50	152	0.01	0	2382	0.08		
"Turkey"	42	-502	0	-25	246	22	1.2	3	4285	0.05		
"Sweden"	38	-1564	0	-36	3	31	0.9	3	2128	0.07		
"Chile"	32	31	0	-54	330	43	0.4	6	11582	0.13		
"Tunisia"	30	-274	0	21	14	78	0.10	-1	3411	0.06		
"Congo"	22	22	0	53	268	154	0.01	-18	6851	0.07		
"Burkina Faso"	21	14	0		-7	136	0.02	5	6109	0.05		
"Hungary"	21	-115	0	116	5	33	0.6	6	2180	0.09		
"Angola"	20	-135	0	-1	-67	100	0.05	-11	7692	0.06		
"Mozambique"	17	17	0	-10	88	108	0.04	8	6196	0.1		
"Senegal"	13	13	0		-6	110	0.04	4	6632	0.05		
"Lithuania"	12	-1823	0	-35	300	59	0.2	7	1630	0.07		
"New Zealand"	11	9	0	-5	875	56	0.2	3	11141	0.09		
"Lao People's Democratic Republic"	10	10	0		-11	129	0.02	-1	2572	0.28		
"Maldives"	8	8	0		592	151	0.01	-3	4478	0.08		
"Estonia"	7	-84	0	-31	-70	73	0.1	6	2314	0.06		
"Argentina"	5	5	0	-48		50	0.3	-5	10631	0.1		
"Brunei Darussalam"	5	5	0	107		109	0.04	26	5185	0.11		
"Egypt"	3	3	0			46	0.3	-1	5219	0.05		
"Peru"	3	3	0	-15	168	54	0.2	3	10420	0.13		
"Iceland"	3	3	0	-31		112	0.04	-1	4251	0.05		
"Guyana"	3	3	0	-81		144	0.02	13	11175	0.21		
"Comoros"	3	3	0	-30	25	192	0	18	5731	0.22		
"Cyprus"	3	2	0		-25	105	0.05	-1	3118	0.09		
"Azerbaijan"	2	2	0			98	0.05	5	3712	0.09		
"Malta"	2	2	0	-35	42	120	0.03	1	3249	0.08		
"Bolivia, Plurinational State of"	1	1	0	116		106	0.04	-3	7978	0.11		
"Pakistan"	1	-1	0	-75		47	0.3	2	4850	0.11		
"Paraguay"	1	-36	0			92	0.06	0	7311	0.15		
"Croatia"		-5				63	0.2	6	1362	0.07		
"Nicaragua"		-14				104	0.05	4	5888	0.1		
"Latvia"		-27				77	0.1	7	1575	0.08		
"India"		-57				10	2.6	2	5793	0.05		
"Morocco"		-67				52	0.3	4	4316	0.07		
"Belarus"		-216				61	0.2	2	2268	0.23		
"Viet Nam"						19	1.5	8	3535	0.19		
"Australia"						24	1.1	1	10262	0.1		
"United Arab Emirates"						29	0.9	-2	5715	0.08		
"Bangladesh"						49	0.3	6	4172	0.15		
"Kazakhstan"						58	0.2	6	3017	0.25		
"Panama"						60	0.2	1	8657	0.12		
"Iraq"						62	0.2	3	4107	0.18		
"Algeria"						64	0.2	-8	4584	0.07		
"Cambodia"						66	0.1	16	2651	0.18		
"Ecuador"						69	0.1	3	9174	0.12		
"Qatar"						70	0.1	0	5745	0.06		
"Iran, Islamic Republic of"						72	0.1	-18	5612	0.13		
"Uzbekistan"						74	0.1	16	3412	0.12		
"Ship stores and bunkers"						75	0.1	-11		0.16		
"Kuwait"						76	0.1	-3	5837	0.07		
"Myanmar"						79	0.10	-1	2536	0.25		
"Kenya"						80	0.09	2	6680	0.08		
"Macao, China"						81	0.09	15		0.17		
"Ghana"						82	0.08	8	8888	0.18		
"Oman"						83	0.08	-6	5977	0.08		
"Jordan"						84	0.08	0	5167	0.07		
"Sri Lanka"						85	0.08	-3	4351	0.16		
"Costa Rica"						86	0.08	3	6987	0.22		
"Liberia"						87	0.07	17	11608	0.19		
"Libya, State of"						89	0.07	12	3475	0.08		
"Marshall Islands"						91	0.06	-1	7455	0.18		
"Côte d'Ivoire"						93	0.06	9	6793	0.08		
"Uruguay"						95	0.06	0	9836	0.11		
"Nepal"						96	0.06	8	1856	0.56		
"Lebanon"						97	0.06	-12	3592	0.06		
"Bahrain"						101	0.05	1	6359	0.08		
"Tanzania, United Republic of"						102	0.05	7	6633	0.1		
"Macedonia, North"						103	0.05	8	1348	0.09		
"Yemen"						107	0.04	11	5706	0.11		
"Congo, Democratic Republic of the"						111	0.04	13	6479	0.14		
"Georgia"						113	0.04	-2	2513	0.09		
"Gibraltar"						114	0.03	-5	2572	0.09		
"Bahamas"						115	0.03	-8	6248	0.19		
"Cameroon"						116	0.03	8	7279	0.14		
"Venezuela, Bolivarian Republic of"						117	0.03	-11	8121	0.16		
"Albania"						121	0.03	6	1886	0.13		
"Sudan"						122	0.03	-2		0.13		
"Cayman Islands"						123	0.03	5	7522	0.35		
"Botswana"						124	0.03	2	2968	0.52		
"Mongolia"						125	0.03	8	3267	0.22		
"Namibia"						126	0.03	-1	3757	0.37		
"Kyrgyzstan"						127	0.03	1	2892	0.2		
"Trinidad and Tobago"						128	0.03	-1	6663	0.26		
"Djibouti"						130	0.02	5	6387	0.22		
"Armenia"						131	0.02	6	3323	0.15		
"Uganda"						132	0.02	11	5788	0.09		
"Mauritius"						133	0.02	-2	7320	0.08		
"Zimbabwe"						134	0.02	7	3754	0.39		
"Guinea"						135	0.02	7	9140	0.19		
"Jamaica"						137	0.02	-2	6759	0.22		
"Madagascar"						138	0.02	2	7339	0.08		
"Syrian Arab Republic"						139	0.02	-3	2496	0.27		
"Haiti"						140	0.02	0	6340	0.18		
"Mali"						141	0.02	9	5008	0.1		
"Papua New Guinea"						143	0.02	-1	4973	0.17		
"Cuba"						145	0.02	-13	7148	0.09		
"Afghanistan"						147	0.02	-8	2990	0.12		
"Turkmenistan"						148	0.02	4	3495	0.15		
"Benin"						149	0.01	-2	6481	0.07		
"Somalia"						150	0.01	9	5305	0.16		
"Togo"						153	0.01	10	7318	0.08		
"New Caledonia"						155	0.01	-1	11471	0.2		
"British Virgin Islands"						156	0.01	8	7242	0.27		
"Gabon"						158	0.01	-2	7114	0.11		
"Rwanda"						159	0.01	17	5437	0.08		
"Eswatini"						160	0	1	1597	0.74		
"Barbados"						161	0	0	4992	0.21		
"Niger"						162	0	13	5715	0.09		
"Curaçao"						163	0	-12		0.24		
"Lesotho"						164	0	-2	1590	0.78		
"French Polynesia"						165	0	1	13300	0.24		
"Gambia"						167	0	7	8260	0.14		
"Malawi"						168	0	0	5169	0.15		
"Bermuda"						169	0	-3	8006	0.28		
"Sierra Leone"						170	0	10	8820	0.13		
"Saint Lucia"						171	0	-10	5321	0.23		
"Faroe Islands"						172	0	7	1404	0.27		
"Suriname"						173	0	1	9050	0.14		
"Antigua and Barbuda"						174	0	1	6334	0.27		
"Aruba"						175	0	-3	4917	0.3		
"Palestine, State of"						176	0	8	3026	0.1		
"United States Minor Outlying Islands"						177	0	0		0.21		
"Bhutan"						178	0	15	1733	0.63		
"Belize"						179	0	1	5006	0.18		
"Greenland"						180	0	6	3482	0.44		
"Cabo Verde"						181	0	4	5196	0.21		
"Equatorial Guinea"						182	0	2	6008	0.11		
"Timor-Leste"						183	0	12	3988	0.22		
"Chad"						184	0	13	6931	0.16		
"Dominica"						185	0	8	4908	0.65		
"Free Zones"						186	0	-34		0.92		
"Seychelles"						187	0	-7	6627	0.06		
"Burundi"						188	0	13	4755	0.14		
"Bonaire, Sint Eustatius and Saba"						189	0	20		0.19		
"Solomon Islands"						190	0	0	6301	0.17		
"Grenada"						191	0	-1	4157	0.2		
"Turks and Caicos Islands"						193	0	-3	3491	0.74		
"South Sudan"						194	0	12		0.25		
"Guinea-Bissau"						195	0	2	5802	0.16		
"Saint Vincent and the Grenadines"						196	0	3	6798	0.14		
"Samoa"						197	0	-1	6816	0.13		
"Vanuatu"						198	0	-2	6248	0.13		
"Central African Republic"						199	0	7	6606	0.08		
"Korea, Democratic People's Republic of"						200	0	-49	1470	0.87		
"Saint Kitts and Nevis"						201	0	-12	5440	0.41		
"Tonga"						202	0	4	5771	0.16		
"Eritrea"						203	0	-8	6303	0.14		
"Tuvalu"						204	0	17	6386	0.21		
"Kiribati"						205	0	10	5936	0.13		
"Cook Islands"						206	0	-4	9417	0.22		
"Falkland Islands (Malvinas)"						207	0	2	12325	0.46		
"Micronesia, Federated States of"						208	0	-8	7109	0.19		
"Palau"						209	0	1	6232	0.16		
"Sao Tome and Principe"						210	0	-3	6155	0.31		
"Nauru"						211	0	18	6014	0.27		
"Anguilla"						212	0	-5	4397	0.58		
"Sint Maarten (Dutch part)"						213	0	-7		0.43		
"French Southern and Antarctic Territories"						214	0	14		0.12		
"Saint Pierre and Miquelon"						215	0	10	3489	0.45		
"Northern Mariana Islands"						216	0	-19	3811	0.2		
"Saint Helena"						217	0	6	7699	0.31		
"Wallis and Futuna Islands"						218	0	7	10103	0.24		
"British Indian Ocean Territory"						219	0	-9		0.67		
"Christmas Island"						220	0	3		0.69		
"Western Sahara"						221	0	-16	12469	0.9		
"Norfolk Island"						222	0	1	5454	0.24		
"Niue"						223	0	7	11410	0.48		
"Montserrat"						224	0	-7		0.49		
"Cocos (Keeling) Islands"						225	0	8		0.74		
"Tokelau"						226	0	-33	11566	0.22		
"Pitcairn"						227	0	-5		0.23		
"West Asia not elsewhere specified"						228	0			1		
"British Antarctic Territory"						229	0			1		
