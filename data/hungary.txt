﻿Importers	Select your indicators-Value exported in 2021 (USD thousand)	Select your indicators-Trade balance 2021 (USD thousand)	Select your indicators-Share in Hungary's exports (%)	Select your indicators-Growth in exported value between 2017-2021 (%, p.a.)	Select your indicators-Growth in exported value between 2020-2021 (%, p.a.)	Select your indicators-Ranking of partner countries in world imports	Select your indicators-Share of partner countries in world imports (%)	Select your indicators-Total imports growth in value of partner countries between 2017-2021 (%, p.a.)	Select your indicators-Average distance between partner countries and all their supplying markets (km)	Select your indicators-Concentration of all supplying countries of partner countries	Select your indicators-Average tariff (estimated) faced by Hungary (%)	
"World"	141256736	2352072	100	4	18		100	3				
"Germany"	37779955	4735672	26.7	4	13	3	6.5	3	3376	0.05		
"Italy"	8272767	2074381	5.9	7	31	11	2.5	2	2891	0.06		
"Romania"	7433452	3517437	5.3	5	19	37	0.5	6	1891	0.07		
"Slovakia"	7268619	-892984	5.1	6	13	38	0.5	4	2664	0.07		
"Austria"	6386358	-2083394	4.5	2	23	27	1	3	1514	0.18		
"Poland"	6054355	-1876283	4.3	5	22	18	1.5	8	3121	0.08		
"France"	5933134	1511682	4.2	3	17	6	3.2	1	3195	0.06		
"Czech Republic"	5788562	-1017698	4.1	2	21	28	1	4	2987	0.1		
"Netherlands"	4684265	-2174489	3.3	3	12	8	2.8	5	3706	0.07		
"United Kingdom"	4427287	3088411	3.1	0	19	7	3.2	1	3930	0.06		
"United States of America"	4149191	1202909	2.9	5	16	1	13.4	3	7938	0.08		
"Spain"	3697563	1618049	2.6	3	12	15	1.9	3	3890	0.05		
"Ukraine"	3278822	1141021	2.3	12	38	48	0.3	7	3255	0.06		
"Belgium"	2916144	-357673	2.1	8	-3	12	2.4	3	2709	0.08		
"Serbia"	2744814	897071	1.9	9	36	65	0.2	9	2497	0.06		
"Croatia"	2548419	658787	1.8	7	38	63	0.2	6	1362	0.07		
"China"	2476385	-7340813	1.8	-3	20	2	11.1	5	6416	0.04		
"Turkey"	2462391	1039632	1.7	3	21	22	1.2	3	4285	0.05		
"Sweden"	2118367	1177644	1.5	6	19	31	0.9	3	2128	0.07		
"Russian Federation"	2069073	-2199340	1.5	2	10	21	1.3	5	4187	0.09		
"Slovenia"	1595124	-212710	1.1	5	22	55	0.2	10	2404	0.07		
"Bulgaria"	1575651	785521	1.1	5	12	57	0.2	6	2237	0.05		
"Switzerland"	1426713	612146	1	10	11	20	1.5	4	3384	0.07		
"Mexico"	867595	482369	0.6	-3	17	13	2.3	2	7039	0.24		
"Denmark"	847696	101324	0.6	-2	12	35	0.6	5	2182	0.08		
"Japan"	830057	-826979	0.6	3	18	4	3.5	1	6256	0.09		
"Portugal"	700830	383978	0.5	8	28	40	0.4	2	2788	0.14		
"Greece"	661306	441311	0.5	9	23	45	0.3	4	2847	0.05		
"Korea, Republic of"	599914	-3579753	0.4	6	25	9	2.8	4	5699	0.09		
"Finland"	515003	265768	0.4	8	15	44	0.4	3	2842	0.07		
"Macedonia, North"	491324	106145	0.3	10	14	103	0.05	8	1348	0.09		
"Australia"	489480	411269	0.3	0	46	24	1.1	1	10262	0.1		
"Bosnia and Herzegovina"	457413	312333	0.3	4	21	94	0.06	3	2228	0.07		
"Ireland"	415953	-304800	0.3	16	22	36	0.5	4	3265	0.1		
"Israel"	398714	173679	0.3	5	4	42	0.4	4	5483	0.07		
"Hong Kong, China"	388394	-1209745	0.3	9	19	5	3.3	3	2841	0.22		
"India"	376246	-206212	0.3	11	33	10	2.6	2	5793	0.05		
"United Arab Emirates"	297966	203440	0.2	11	20	29	0.9	-2	5715	0.08		
"Lithuania"	296645	75526	0.2	0	16	59	0.2	7	1630	0.07		
"Egypt"	292051	189659	0.2	25	36	46	0.3	-1	5219	0.05		
"Singapore"	289505	-328567	0.2	11	35	16	1.9	3	5773	0.07		
"Canada"	285566	196850	0.2	-3	15	14	2.2	1	5042	0.26		
"Norway"	270019	238391	0.2	9	35	39	0.5	3	3470	0.06		
"Brazil"	254755	153437	0.2	-1	39	26	1	6	10879	0.09		
"Latvia"	244384	146697	0.2	4	16	77	0.1	7	1575	0.08		
"South Africa"	224257	189252	0.2	-2	41	41	0.4	-1	9230	0.07		
"Argentina"	206767	189474	0.1	14	93	50	0.3	-5	10631	0.1		
"Morocco"	188493	119325	0.1	-1	23	52	0.3	4	4316	0.07		
"Taipei, Chinese"	179543	-797146	0.1	-3	-6	17	1.7	8	5007	0.1		
"Malaysia"	172613	-500415	0.1	-1	19	25	1.1	3	5172	0.09		
"Luxembourg"	163837	-7186	0.1	15	20	71	0.1	3	1488	0.14		
"Thailand"	158649	-339552	0.1	11	1	23	1.2	2	4905	0.09		
"Kazakhstan"	151659	-117541	0.1	4	-8	58	0.2	6	3017	0.25		
"Saudi Arabia"	147008	104207	0.1	6	4	32	0.7	3	5816	0.07		
"Belarus"	137781	17886	0.1	1	24	61	0.2	2	2268	0.23		
"Estonia"	131843	73987	0.1	-7	33	73	0.1	6	2314	0.06		
"Colombia"	124601	123416	0.1	18	153	51	0.3	4	8915	0.13		
"Albania"	116238	93507	0.1	2	13	121	0.03	6	1886	0.13		
"Uzbekistan"	112362	111169	0.1	25	-4	74	0.1	16	3412	0.12		
"Tunisia"	107647	-31902	0.1	-1	16	78	0.10	-1	3411	0.06		
"Moldova, Republic of"	107060	53217	0.1	-1	13	118	0.03	8	2317	0.08		
"Viet Nam"	102615	-870822	0.1	7	10	19	1.5	8	3535	0.19		
"Chile"	74880	66057	0.1	1	19	43	0.4	6	11582	0.13		
"Montenegro"	74459	64085	0.1	13	14	152	0.01	0	2382	0.08		
"Georgia"	73056	71468	0.1	7	48	113	0.04	-2	2513	0.09		
"New Zealand"	67316	64422	0	-3	29	56	0.2	3	11141	0.09		
"Cyprus"	66479	1334	0	3	33	105	0.05	-1	3118	0.09		
"Qatar"	59622	58027	0	17	63	70	0.1	0	5745	0.06		
"Azerbaijan"	48595	47405	0	-3	15	98	0.05	5	3712	0.09		
"Algeria"	46401	46051	0	-5	14	64	0.2	-8	4584	0.07		
"Iraq"	45772	44837	0	-1	4	62	0.2	3	4107	0.18		
"Costa Rica"	41476	25716	0	4	54	86	0.08	3	6987	0.22		
"Indonesia"	38665	-106595	0	-5	9	30	0.9	2	5872	0.11		
"Iran, Islamic Republic of"	38623	28807	0	-6	31	72	0.1	-18	5612	0.13		
"Kuwait"	38414	36358	0	12	-14	76	0.1	-3	5837	0.07		
"Jordan"	32036	30139	0	-1	-8	84	0.08	0	5167	0.07		
"Peru"	30455	29739	0	-5	67	54	0.2	3	10420	0.13		
"Afghanistan"	27940	27586	0	54	104	147	0.02	-8	2990	0.12		
"Bolivia, Plurinational State of"	27877	27837	0	-3	306	106	0.04	-3	7978	0.11		
"Pakistan"	27161	-7921	0	-11	27	47	0.3	2	4850	0.11		
"Nigeria"	26818	23289	0	-10	46	53	0.2	17	7792	0.1		
"Lebanon"	23001	21631	0	-21	-17	97	0.06	-12	3592	0.06		
"Bahrain"	21069	17664	0	40	21	101	0.05	1	6359	0.08		
"Uruguay"	20864	18683	0	34	142	95	0.06	0	9836	0.11		
"Mongolia"	20364	20082	0	18	65	125	0.03	8	3267	0.22		
"Philippines"	20100	-155733	0	-22	-19	34	0.6	2	4354	0.09		
"Armenia"	19795	18830	0	5	-1	131	0.02	6	3323	0.15		
"Malta"	18420	-57839	0	-7	-3	120	0.03	1	3249	0.08		
"Ecuador"	17521	17192	0	-16	104	69	0.1	3	9174	0.12		
"Ghana"	16213	15967	0	3	18	82	0.08	8	8888	0.18		
"Côte d'Ivoire"	15587	-15922	0	26	-9	93	0.06	9	6793	0.08		
"Bangladesh"	13359	-2479	0	9	52	49	0.3	6	4172	0.15		
"Panama"	13055	11293	0	-11	24	60	0.2	1	8657	0.12		
"Ethiopia"	12751	10928	0	1	24	90	0.07	1	6305	0.11		
"Libya, State of"	12706	-4508	0	4	31	89	0.07	12	3475	0.08		
"Kyrgyzstan"	12349	12276	0	7	47	127	0.03	1	2892	0.2		
"Uganda"	11039	10540	0	10	70	132	0.02	11	5788	0.09		
"Oman"	10875	9662	0	-9	24	83	0.08	-6	5977	0.08		
"Iceland"	10828	-33692	0	-3	-3	112	0.04	-1	4251	0.05		
"Cameroon"	9777	9296	0	5	14	116	0.03	8	7279	0.14		
"Turkmenistan"	9197	8234	0	-12	-34	148	0.02	4	3495	0.15		
"Jamaica"	8757	8733	0	-10	-9	137	0.02	-2	6759	0.22		
"Angola"	8407	8391	0	-39	105	100	0.05	-11	7692	0.06		
"Kenya"	8105	7244	0	-6	-10	80	0.09	2	6680	0.08		
"Senegal"	7610	7558	0	-20	-6	110	0.04	4	6632	0.05		
"Dominican Republic"	6485	6309	0	-4	39	67	0.1	5	6446	0.22		
"Syrian Arab Republic"	6443	6333	0	17	0	139	0.02	-3	2496	0.27		
"Trinidad and Tobago"	5945	5945	0	-19	5	128	0.03	-1	6663	0.26		
"Lao People's Democratic Republic"	5523	5518	0	21	-56	129	0.02	-1	2572	0.28		
"Rwanda"	5019	4979	0	48	394	159	0.01	17	5437	0.08		
"Paraguay"	4750	4729	0	-13	43	92	0.06	0	7311	0.15		
"Yemen"	4577	4577	0	-32	-41	107	0.04	11	5706	0.11		
"Botswana"	4558	4507	0	28	147	124	0.03	2	2968	0.52		
"Benin"	4539	4539	0	-13	151	149	0.01	-2	6481	0.07		
"Sri Lanka"	4515	-36974	0	-18	-11	85	0.08	-3	4351	0.16		
"Honduras"	3849	3593	0	-10	84	99	0.05	4	6104	0.16		
"Brunei Darussalam"	3556	3553	0	1	-29	109	0.04	26	5185	0.11		
"Guinea"	3430	3423	0	18	-38	135	0.02	7	9140	0.19		
"Guatemala"	3388	3375	0	0	94	68	0.1	7	5828	0.15		
"Tanzania, United Republic of"	2886	2114	0	-18	163	102	0.05	7	6633	0.1		
"Tajikistan"	2695	2695	0	-1	64	142	0.02	9	2803	0.17		
"Nicaragua"	2650	1527	0	-2	20	104	0.05	4	5888	0.1		
"Palestine, State of"	2569	2518	0	-45		176	0	8	3026	0.1		
"Mauritania"	2447	2426	0	4	174	146	0.02	1	5817	0.07		
"Congo, Democratic Republic of the"	2296	2287	0	4	90	111	0.04	13	6479	0.14		
"Zambia"	2291	2180	0	4	185	119	0.03	-9	5447	0.14		
"Macao, China"	2282	-3397	0	6	135	81	0.09	15		0.17		
"New Caledonia"	2203	2200	0	0	8	155	0.01	-1	11471	0.2		
"Sudan"	2151	2147	0	0	-3	122	0.03	-2		0.13		
"Barbados"	2146	2132	0	-17	-18	161	0	0	4992	0.21		
"Congo"	2132	2065	0	-13	8	154	0.01	-18	6851	0.07		
"Area Nes"	2047	2047	0	-36	4							
"Mauritius"	2013	1427	0	-24	10	133	0.02	-2	7320	0.08		
"French Polynesia"	2004	2004	0	-8	101	165	0	1	13300	0.24		
"Liberia"	1913	1911	0	1	73	87	0.07	17	11608	0.19		
"Mozambique"	1734	-2065	0	-4	112	108	0.04	8	6196	0.1		
"Burkina Faso"	1568	1239	0	-21	40	136	0.02	5	6109	0.05		
"El Salvador"	1561	1543	0	2	5	88	0.07	9	4914	0.15		
"Gabon"	1503	1486	0	9	3	158	0.01	-2	7114	0.11		
"Madagascar"	1423	1117	0	-20	417	138	0.02	2	7339	0.08		
"Nepal"	1362	849	0	-5	192	96	0.06	8	1856	0.56		
"Cambodia"	1214	-936	0	4	30	66	0.1	16	2651	0.18		
"Togo"	1180	1167	0	-10	21	153	0.01	10	7318	0.08		
"Niger"	1134	1093	0	11	-38	162	0	13	5715	0.09		
"Guyana"	1118	761	0	34	-17	144	0.02	13	11175	0.21		
"Haiti"	1080	1080	0	-18	32	140	0.02	0	6340	0.18		
"Djibouti"	1003	813	0	-9	-15	130	0.02	5	6387	0.22		
"Mali"	1001	959	0	-30	-51	141	0.02	9	5008	0.1		
"Saint Lucia"	961	961	0	-1	22	171	0	-10	5321	0.23		
"Somalia"	944	934	0	88	105	150	0.01	9	5305	0.16		
"Curaçao"	906	903	0	-4	-40	163	0	-12		0.24		
"Venezuela, Bolivarian Republic of"	855	853	0	-21	-6	117	0.03	-11	8121	0.16		
"Chad"	851	847	0	31	-26	184	0	13	6931	0.16		
"Maldives"	816	815	0	62	768	151	0.01	-3	4478	0.08		
"Suriname"	786	706	0	-8	108	173	0	1	9050	0.14		
"Sierra Leone"	782	772	0	-5	134	170	0	10	8820	0.13		
"Myanmar"	781	-1353	0	-21	25	79	0.10	-1	2536	0.25		
"Bermuda"	732	591	0	-16	147	169	0	-3	8006	0.28		
"Africa not elsewhere specified"	614	609	0	25	54							
"Gambia"	578	578	0	-22	18	167	0	7	8260	0.14		
"Cuba"	555	413	0	-32	-39	145	0.02	-13	7148	0.09		
"Gibraltar"	554	376	0	-28	5	114	0.03	-5	2572	0.09		
"Sint Maarten (Dutch part)"	445	431	0	-13	4	213	0	-7		0.43		
"Zimbabwe"	437	-3868	0	-29	63	134	0.02	7	3754	0.39		
"Cabo Verde"	416	402	0	14	540	181	0	4	5196	0.21		
"Malawi"	416	73	0	-23	92	168	0	0	5169	0.15		
"Antigua and Barbuda"	408	408	0	-12	9	174	0	1	6334	0.27		
"United States Minor Outlying Islands"	392	392	0	63	46	177	0	0		0.21		
"Equatorial Guinea"	380	380	0	42	-13	182	0	2	6008	0.11		
"Dominica"	370	370	0	35	-38	185	0	8	4908	0.65		
"Cayman Islands"	322	-234	0	-13	246	123	0.03	5	7522	0.35		
"Aruba"	283	277	0	-18	-24	175	0	-3	4917	0.3		
"Seychelles"	254	-1313	0	-33	72	187	0	-7	6627	0.06		
"Guinea-Bissau"	253	253	0	14	229	195	0	2	5802	0.16		
"Burundi"	249	249	0	44	-18	188	0	13	4755	0.14		
"Saint Kitts and Nevis"	247	247	0	0	55	201	0	-12	5440	0.41		
"Bahamas"	183	183	0	3	-46	115	0.03	-8	6248	0.19		
"Grenada"	174	174	0	-31	91	191	0	-1	4157	0.2		
"Central African Republic"	163	67	0	102	68	199	0	7	6606	0.08		
"British Virgin Islands"	162	133	0	-30	-53	156	0.01	8	7242	0.27		
"Faroe Islands"	161	157	0	-20	-3	172	0	7	1404	0.27		
"Palau"	155	155	0	8		209	0	1	6232	0.16		
"Namibia"	154	8	0	-14	5	126	0.03	-1	3757	0.37		
"Andorra"	136	115	0	11	178	166	0	2	610	0.52		
"French Southern and Antarctic Territories"	94	94	0	186	-10	214	0	14		0.12		
"Vanuatu"	85	85	0	0	-25	198	0	-2	6248	0.13		
"Turks and Caicos Islands"	82	82	0	-27	95	193	0	-3	3491	0.74		
"Saint Vincent and the Grenadines"	82	82	0	25	78	196	0	3	6798	0.14		
"Comoros"	71	71	0			192	0	18	5731	0.22		
"Fiji"	61	27	0	-15	-23	157	0.01	-7	7384	0.12		
"Papua New Guinea"	60	34	0	-4	0	143	0.02	-1	4973	0.17		
"South Sudan"	37	37	0			194	0	12		0.25		
"Bhutan"	30	30	0	15	-77	178	0	15	1733	0.63		
"Greenland"	23	18	0	-42	1050	180	0	6	3482	0.44		
"Lesotho"	14	-7	0			164	0	-2	1590	0.78		
"Belize"	14	-771	0	-29	-96	179	0	1	5006	0.18		
"Northern Mariana Islands"	11	11	0			216	0	-19	3811	0.2		
"Samoa"	11	-1123	0			197	0	-1	6816	0.13		
"Eswatini"	6	-54	0	8	-82	160	0	1	1597	0.74		
"Korea, Democratic People's Republic of"	3	-6	0		50	200	0	-49	1470	0.87		
"Nauru"	1	1	0			211	0	18	6014	0.27		
"Timor-Leste"		-7				183	0	12	3988	0.22		
"Saint Helena"		-32				217	0	6	7699	0.31		
"Ship stores and bunkers"						75	0.1	-11		0.16		
"Marshall Islands"						91	0.06	-1	7455	0.18		
"Free Zones"						186	0	-34		0.92		
"Bonaire, Sint Eustatius and Saba"						189	0	20		0.19		
"Solomon Islands"						190	0	0	6301	0.17		
"Tonga"						202	0	4	5771	0.16		
"Eritrea"						203	0	-8	6303	0.14		
"Tuvalu"						204	0	17	6386	0.21		
"Kiribati"						205	0	10	5936	0.13		
"Cook Islands"						206	0	-4	9417	0.22		
"Falkland Islands (Malvinas)"						207	0	2	12325	0.46		
"Micronesia, Federated States of"						208	0	-8	7109	0.19		
"Sao Tome and Principe"						210	0	-3	6155	0.31		
"Anguilla"						212	0	-5	4397	0.58		
"Saint Pierre and Miquelon"						215	0	10	3489	0.45		
"Wallis and Futuna Islands"						218	0	7	10103	0.24		
"British Indian Ocean Territory"						219	0	-9		0.67		
"Christmas Island"						220	0	3		0.69		
"Western Sahara"						221	0	-16	12469	0.9		
"Norfolk Island"						222	0	1	5454	0.24		
"Niue"						223	0	7	11410	0.48		
"Montserrat"						224	0	-7		0.49		
"Cocos (Keeling) Islands"						225	0	8		0.74		
"Tokelau"						226	0	-33	11566	0.22		
"Pitcairn"						227	0	-5		0.23		
"West Asia not elsewhere specified"						228	0			1		
"British Antarctic Territory"						229	0			1		
